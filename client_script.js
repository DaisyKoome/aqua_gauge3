function get_client_details(client_id) {
 
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var myObj = JSON.parse(this.responseText);
        alert(this.responseText);
        document.getElementById("c_id").value = myObj.id;
        document.getElementById("n_id").value = myObj.n_id;
        document.getElementById("fname").value = myObj.fname;
        document.getElementById("lname").value = myObj.lname;
        document.getElementById("email").value = myObj.email;
        document.getElementById("phone_no").value = myObj.phone_no;
        document.getElementById("location5").value = myObj.location5;
        document.getElementById("status5").value = myObj.status5;
        document.getElementById("vendor_id").value = myObj.vendor_id;
        document.getElementById("sensor_id").value = myObj.sensor_id;
        //document.getElementById("cust_id").value = myObj.id;

        var location5;
        var status5;
        
        if (myObj.location5=="Kitengela") location5="Kitengela";
        else if (myObj.location5=="Isinya") location5="Isinya";
        else location5="Kajiado town";
        document.getElementById("location5").value = location5;
        
        if (myObj.status5=="Approved") status5="Approved";
        else if (myObj.status5=="Denied") status5="Denied";
        else status5="Pending";
        document.getElementById("status5").value = status5;

        //Display modal
        var exampleModal = document.getElementById('exampleModal');
        exampleModal.addEventListener('show.bs.modal', function (event) {
          // Button that triggered the modal
          var button = event.relatedTarget;
          // Extract info from data-bs-* attributes
          var recipient = button.getAttribute('data-bs-whatever');
          // If necessary, you could initiate an AJAX request here
          // and then do the updating in a callback.
          //
          // Update the modal's content.
          var modalTitle = exampleModal.querySelector('.modal-title');
          var modalBodyInput = exampleModal.querySelector('.modal-body input');

          modalTitle.textContent = 'Edit client ';
          modalBodyInput.value = recipient;
        })
      }
    };
    xhttp.open("GET", "client_ajax.php?client_id=" + client_id, true);
    xhttp.send();
  }