<?php
echo "<html>
    <head>
    <title>Aquagauge water report</title>
    <link href='https://fonts.googleapis.com/css2?family=Quicksand&display=swap' rel='stylesheet'>
    </head>
    <body>
    <div 
    style='font-family: Quicksand; font-size: 10px;  border: solid 6px #fff;
      border-radius: 10px;
      background: #f5f5f5;
      box-shadow: 0 30px 20px -20px rgba(0,0,0,0.3);
      box-sizing: border-box;  position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      width: 40%;'>
    <div style='position: relative;
    width: 50%;
    height: 100%;  float: left;
    overflow: hidden;'>
    <img src='dist/img/logo4.png' alt='Logo' height='70' width='210' 
    style='margin-top: 40%;margin-left: 10%; margin-bottom: 20%; border-radius: 15px;'/>
    </div>
    <div style='position: relative;
    width: 50%;
    height: 100%; float: right;
    padding: 2rem 1rem;
    box-sizing: border-box;'>

    <div style='padding: 1rem 0;
    font-size: 1.5rem;
    font-weight: bold;'>Dear Client,</div>

    <div style='font-weight: bold; font-size: 1.2rem;
    line-height: 2rem;'>
    Day, Date
    </div>

    <div style='font-size: 1rem;
    line-height: 2rem;'>
    Your water tank report is as follows:
    </div>

    <div style='margin-left: 5px; font-weight: bold; font-size: 1rem;
    line-height: 2rem;'>
    ~ Consumption:
    <br>
    ~ Water level:
    </div>

  </div>
</div>
    
    </body>
    </html>";
?>