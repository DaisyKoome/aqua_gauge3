<?php
session_start();
if(isset($_SESSION['clientlogin'])){
    header("Location: clientlogin.php");
}
if(isset($_SESSION['vendorlogin'])){
    header("Location: clientlogin.php");
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/logo.png">
    <title>Registration</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/gui_reg.css">

</head>

<body>    
    <div class="container main_cont">
        <div class="row no-gutters">
            <div class="col-8 col-lg-6 ml-auto mr-auto order-6">
                 
                <div class="alert alert-info alert1" role="alert" style="min-height: 100px;">
                    <img src="img/typing.png" class="img-fluid img1" alt="Avatar">
                    <div>
                        <h3 class="text-center">Register with us!</h3>
                    </div>                    
                    <form method="post" action="server.php">
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <?php
            
                                if(isset($_SESSION['errors'])){
                                    echo "<div class='alert alert-danger'>";
                                    for ($i=0; $i < sizeof($_SESSION['errors']); $i++) { 
                                        echo "*".$_SESSION['errors'][$i]."<br>";
                                    }
                                    echo '</div>';
                                    unset($_SESSION['errors']);
                                }
                            ?>
                        </div> 
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">National ID</label>
                            <input type="text" class="form-control" name="n_id" placeholder="Enter your national ID">
                        </div>                        
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">First name</label>
                            <input type="text" class="form-control" name="fname" placeholder="Enter your first name">
                        </div>
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">Last name</label>
                            <input type="text" class="form-control" name="lname" placeholder="Enter your last name">
                        </div>
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter your email">
                        </div>
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Enter your password">
                        </div>
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">Phone number</label>
                            <input type="text" class="form-control" name="pno" placeholder="Enter your phone number">
                        </div>
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">Location</label>
                            <input type="text" class="form-control" name="location" placeholder="Enter your location">
                        </div>
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">Are you registering as a client or water vendor?</label>
                            <select class="form-control" name="client_type">
                                <option value="client">Client</option>
                                <option value="vendor">Water vendor</option>
                            </select>
                        </div>                        
                        <div class="form-group text-center">
                            <input type="submit" class="btn btn-info text-center" name="register" value="Register">
                        </div>
                        <div>
                            <h5 class="text-center" style="text-decoration: none; font-size: 15px;">Already registered? 
                                <a href="clientlogin.php" style="text-decoration: none; font-size: 15px;">Log in</a> here
                            </h5>                          
                        </div>
                        <div>
                            <h5 class="text-center" style="text-decoration: none; font-size: 15px;"> 
                                <a href="index.php" style="text-decoration: none; font-size: 15px;">
                                    Back to home
                                </a>
                            </h5>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

</body>


</html>