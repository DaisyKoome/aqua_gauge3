<?php
require '../vendor/autoload.php';
use AfricasTalking\SDK\AfricasTalking;

// Set your app credentials
$username   = "daisyk";
$apiKey     = "b57e5f3527ce43adb2c9958153a388e115f41af370ed1efbd31c5ec744562089";

// Initialize the SDK
$AT         = new AfricasTalking($username, $apiKey);

// Get the SMS service
//uncomment the next line to make the code work
$sms        = $AT->sms();

// Set the numbers you want to send to in international format
$recipients = "+254799448090";

// Set your message
$message    = "Dear Client, your tank is 75% empty. It needs a refill";

// Set your shortCode or senderId
//$from       = "myShortCode or mySenderId";

try {
    // Thats it, hit send and we'll take care of the rest
    $result = $sms->send([
        'to'      => $recipients,
        'message' => $message,
        //'from'    => $from
    ]);

    print_r($result);
} catch (Exception $e) {
    echo "Error: ".$e->getMessage();
}
?>