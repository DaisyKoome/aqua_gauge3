$(document).ready(function(){
    $.ajax({
      url: "http://localhost/aqua_gauge/chartjs/php/data.php",
      method: "GET",
      success: function(data) {
        console.log(data);
        var device = [];
        var water_level = [];
  
        for(var i in data) {
          device.push("Device " + data[i].device_id);
          water_level.push(data[i].water_level);
        }
  
        var chartdata = {
          labels: device,
          datasets : [
            {
              label: 'Water consumption',
              backgroundColor: 'rgba(200, 200, 200, 0.75)',
              borderColor: 'rgba(200, 200, 200, 0.75)',
              hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
              hoverBorderColor: 'rgba(200, 200, 200, 1)',
              data: water_level
            }
          ]
        };
  
        var ctx = $("#mycanvas");
  
        var barGraph = new Chart(ctx, {
          type: 'bar',
          data: chartdata
        });
      },
      error: function(data) {
        console.log(data);
      }
    });
  });