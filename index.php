<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="dist/img/logo.png">
    <title>Home - Aqua_Gauge</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">

    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
    

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/album/album.css" rel="stylesheet">

    <!-- Own CSS -->
    <link rel="stylesheet" type="text/css" href="css/gui_index.css">
  </head>

  <body>
  <!-- Navbar -->
  <nav class="navbar navbar-dark bg-dark">
  <div class="container" >
    <a class="navbar-brand" href="#">
      <img src="dist/img/logo.png" alt="" width="30" height="24">
      <span style="color: #98cce6;">AQUA_GAUGE.</span>
      <span style="color: #98cce6; font-size: 15px;"><strong>Water is life.</strong></span>
    </a>
  </div>
</nav>

  <!-- Main body - About section -->
    <main role="main">

      <div class="container my-5">
        <div class="row p-4 pb-0 pe-lg-0 pt-lg-5 align-items-center rounded-3 border shadow-lg">
          <div class="col-lg-7 p-3 p-lg-5 pt-lg-3">
            <h1 class="display-4 fw-bold lh-1" style="font-size:35px;">At Aqua_Gauge, we're all about water management in the home</h1>
            <ul class="lead">
              <li> 
              Is your water tank always running out without your knowledge? <br>
              Just <a href="registration.php" style="text-decoration: none;">sign up</a> here
              </li>
              <br>
              <li id="about">
              Would you want to know how much water you use per day? <br>
              All you need to do is <a href="registration.php" style="text-decoration: none;">sign up</a> here
              </li>
              <br>
              <li>
              Do you rely on water vendors to supply your water? <br>
              <a href="registration.php" style="text-decoration: none;">Sign up</a> here
              </li>
              <br>
              <li>
              Are you a water vendor? <br>
              <a href="registration.php" style="text-decoration: none;">Register</a> here
              </li>
            </ul>
          </div>
          <div class="col-lg-4 offset-lg-1 p-0 position-relative overflow-hidden shadow-lg">
            <div class="position-lg-absolute top-0 left-0 overflow-hidden">
              <img class="d-block rounded-lg-3" src="img/water-drop.png" alt=""style="margin-left: 50px;" width="280" height="250">
            </div>
            <br><br>
            <div class="position-lg-absolute top-0 left-0 overflow-hidden">
              <img class="d-block rounded-lg-3" style="margin-left: 80px;"
              src="img/sms.png" alt="" width="200" height="250">
            </div>
            <br><br>
          </div>
        </div>
      </div>

<!-- Footer -->
      <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" style="background-color: #212529;">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner" id="contacts">
          <div class="carousel-item active">
            <img src="img/contacts.png" width="180px" height="120px" class="d-block w-20" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h4>Call/SMS this number for assistance:</h4>
              <h5>0799448090</h5>
            </div>
          </div>
          <div class="carousel-item">
            <img src="img/gmail.png" width="180px" height="120px" class="d-block w-20" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h4>Send an email to:</h4>
              <h5>dskoome2@gmail.com</h5>
            </div>
          </div>
          <div class="carousel-item">
            <img src="img/twitter.png" width="180px" height="120px" class="d-block w-20" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h4>Twitter handle:</h4>
              <h5>@aqua_gauge</h5>
            </div>
          </div>
        </div>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"><\/script>')</script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
  </body>
</html>
