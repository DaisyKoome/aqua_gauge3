<?php
session_start();
if(isset($_SESSION['clientlogin'])){
    header("Location: client.php");
}
if(isset($_SESSION['vendorlogin'])){
    header("Location: vendor.php");
}
if(isset($_SESSION['adminlogin'])){
    header("Location: index_sensors.php");
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/logo.png">
    <title>Log in</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
    
    <!-- Own CSS -->
    <link rel="stylesheet" type="text/css" href="css/gui_login.css">

</head>

<body>
    
    <div class="container main_cont">
        <div class="row no-gutters">
            <div class="col-6 col-lg-4 ml-auto mr-auto order-6">
                 
                <div class="alert alert-info alert1" role="alert" style="min-height: 100px;">
                    <img src="img/typing.png" class="img-fluid img1" alt="Avatar">
                    <div>
                    <h3 class="text-center">Welcome. Karibu!</h3>
                    </div>
                    <form method="post" action="server.php">
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <?php
                                if(isset($_SESSION['error'])){
                                    echo "<div class='alert alert-danger'>";
                                        for ($i=0; $i < sizeof($_SESSION['error']); $i++) { 
                                        echo "*".$_SESSION['error'][$i]."<br>";
                                        }
                                    echo '</div>';
                                    unset($_SESSION['error']);
                                }
                            ?>
                        </div>                                            
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter your username">
                        </div>
                        <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
                            <label for="exampleFormControlInput1">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Enter your password">
                        </div>
                        <div class="form-group text-center">
                            <input type="submit" class="btn btn-info text-center" name="login" value="Log in">
                        </div>
                        <div>
                            <h5 class="text-center" style="text-decoration: none; font-size: 15px;">Not registered? 
                                <a href="registration.php" style="text-decoration: none; font-size: 15px;">Register here</a> 
                            </h5>
                        </div>
                        <div>
                            <h5 class="text-center" style="text-decoration: none; font-size: 15px;"> 
                                <a href="index.php" style="text-decoration: none; font-size: 15px;">
                                    Back to home
                                </a>
                            </h5>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

</body>


</html>