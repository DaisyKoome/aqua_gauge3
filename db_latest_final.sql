-- MySQL dump 10.13  Distrib 8.0.25, for Linux (x86_64)
--
-- Host: localhost    Database: aquagauge2
-- ------------------------------------------------------
-- Server version	8.0.25-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `admin_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'Daisy','Koome','dskoome2@gmail.com','df4b892324bbb648f27734b55c206b4b'),(2,'helen','mwende','helen@gmail.com','7a2eb41a38a8f4e39c1586649da21e5f'),(3,'John','Kibet','john@gmail.com','527bd5b5d689e2c32ae974c6229ff785'),(4,'harry','otieno','harry@gmail.com','3b87c97d15e8eb11e51aa25e9a5770e9');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `client_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `phone_no` int DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'daisy','koome','daisymitti@gmail.com','94464f01299f0de04e924912b47a80f4',712345678,'nakuru','Pending'),(2,'ned','kamau','nedkamau@gmail.com','f68daad189b2fffd0b8cab5e36ec9d96',798765432,'juja','Denied'),(3,'ken','mutie','ken@gmail.com','f632fa6f8c3d5f551c5df867588381ab',799448090,'kjd','Approved'),(4,'dee','dee','dee@gmail.com','f31f2f4e88b1f4c29a4542671b247f9b',799448090,'kjd','Pending'),(5,'daisy','mitti','mitti@gmail.com','8744c53672906143e20538f6ac3deadb',799448090,'maua','Pending'),(6,'nick','kim','nick@gmail.com','e2e42a07550863f8b67f5eb252581f6d',799448090,'kitui',NULL);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_sensor`
--

DROP TABLE IF EXISTS `client_sensor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `client_sensor` (
  `client_sensor_id` int NOT NULL AUTO_INCREMENT,
  `client_id` int DEFAULT NULL,
  `sensor_id` int DEFAULT NULL,
  PRIMARY KEY (`client_sensor_id`),
  KEY `client_id` (`client_id`),
  KEY `sensor_id` (`sensor_id`),
  CONSTRAINT `client_sensor_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `client_sensor_ibfk_2` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`sensor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_sensor`
--

LOCK TABLES `client_sensor` WRITE;
/*!40000 ALTER TABLE `client_sensor` DISABLE KEYS */;
INSERT INTO `client_sensor` VALUES (1,2,3),(2,3,1),(3,4,2);
/*!40000 ALTER TABLE `client_sensor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_vendor`
--

DROP TABLE IF EXISTS `client_vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `client_vendor` (
  `client_vendor_id` int NOT NULL AUTO_INCREMENT,
  `client_id` int DEFAULT NULL,
  `vendor_id` int DEFAULT NULL,
  PRIMARY KEY (`client_vendor_id`),
  KEY `client_id` (`client_id`),
  KEY `vendor_id` (`vendor_id`),
  CONSTRAINT `client_vendor_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  CONSTRAINT `client_vendor_ibfk_2` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`vendor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_vendor`
--

LOCK TABLES `client_vendor` WRITE;
/*!40000 ALTER TABLE `client_vendor` DISABLE KEYS */;
INSERT INTO `client_vendor` VALUES (1,1,2),(2,2,3);
/*!40000 ALTER TABLE `client_vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data`
--

DROP TABLE IF EXISTS `data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `data` (
  `data_id` int NOT NULL AUTO_INCREMENT,
  `sensor_id` int DEFAULT NULL,
  `water_level` int DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`data_id`),
  KEY `sensor_id` (`sensor_id`),
  CONSTRAINT `data_ibfk_1` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`sensor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data`
--

LOCK TABLES `data` WRITE;
/*!40000 ALTER TABLE `data` DISABLE KEYS */;
INSERT INTO `data` VALUES (1,1,76,'2021-06-18 00:42:26'),(2,2,49,'2021-06-18 00:42:44'),(3,3,54,'2021-06-18 00:42:56'),(4,4,32,'2021-06-18 00:43:17'),(5,5,34,'2021-06-18 08:22:45'),(8,2,11,'2021-06-18 09:55:14'),(9,3,16,'2021-06-18 09:59:29'),(10,2,67,'2021-06-18 10:27:06'),(11,2,67,'2021-06-18 10:27:47'),(12,1,38,'2021-06-18 10:28:05'),(13,3,55,'2021-06-21 09:21:19');
/*!40000 ALTER TABLE `data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor`
--

DROP TABLE IF EXISTS `sensor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `sensor` (
  `sensor_id` int NOT NULL AUTO_INCREMENT,
  `serial_no` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`sensor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor`
--

LOCK TABLES `sensor` WRITE;
/*!40000 ALTER TABLE `sensor` DISABLE KEYS */;
INSERT INTO `sensor` VALUES (1,'D001'),(2,'D002'),(3,'D003'),(4,'D004'),(5,'D005');
/*!40000 ALTER TABLE `sensor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensor_vendor`
--

DROP TABLE IF EXISTS `sensor_vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `sensor_vendor` (
  `sensor_vendor_id` int NOT NULL AUTO_INCREMENT,
  `vendor_id` int DEFAULT NULL,
  `sensor_id` int DEFAULT NULL,
  PRIMARY KEY (`sensor_vendor_id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `sensor_id` (`sensor_id`),
  CONSTRAINT `sensor_vendor_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`vendor_id`),
  CONSTRAINT `sensor_vendor_ibfk_2` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`sensor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensor_vendor`
--

LOCK TABLES `sensor_vendor` WRITE;
/*!40000 ALTER TABLE `sensor_vendor` DISABLE KEYS */;
INSERT INTO `sensor_vendor` VALUES (1,2,3),(2,2,1),(3,3,4),(4,1,5),(5,4,2);
/*!40000 ALTER TABLE `sensor_vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tank`
--

DROP TABLE IF EXISTS `tank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `tank` (
  `tank_id` int NOT NULL AUTO_INCREMENT,
  `sensor_id` int DEFAULT NULL,
  `tank_name_alias` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`tank_id`),
  KEY `sensor_id` (`sensor_id`),
  CONSTRAINT `tank_ibfk_1` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`sensor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tank`
--

LOCK TABLES `tank` WRITE;
/*!40000 ALTER TABLE `tank` DISABLE KEYS */;
INSERT INTO `tank` VALUES (1,1,'kajiado'),(2,2,'kitui'),(3,3,'nakuru'),(4,4,'kisumu');
/*!40000 ALTER TABLE `tank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `vendor` (
  `vendor_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `phone_no` int DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`vendor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor`
--

LOCK TABLES `vendor` WRITE;
/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
INSERT INTO `vendor` VALUES (1,'karen','oloo','karen@gmail.com','ba952731f97fb058035aa399b1cb3d5c',712345678,'narok','Pending'),(2,'liam','maina','liammaina@gmail.com','6d8c4d103f90154cc06dd75a71d061be',756439801,'kitale','Approved'),(3,'daisy','koome','daisy@gmail.com','df4b892324bbb648f27734b55c206b4b',799448090,'kjd','Denied'),(4,'natalie','kimani','nataliekimani@gmail.com','f9e1f3ae15198f819fbc3449479401bf',708739801,'wajir','Pending'),(5,'daisy','mitti','dee2@gmail.com','8744c53672906143e20538f6ac3deadb',708543211,'maua','Pending');
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-25  4:17:45
