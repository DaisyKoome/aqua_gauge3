function get_waterlevel_details(data_id) {
 
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {

        var sensorObject = JSON.parse(this.responseText);
        alert (this.responseText);
        document.getElementById("data_id").value = sensorObject.data_id;
        document.getElementById("d_id4").value = sensorObject.id2;
        document.getElementById("wlevel2").value = sensorObject.wlevel2;
        document.getElementById("dev_id4").value = sensorObject.id2;
        //console.log(sensorObject.id2);

        //Display modal
        var editsensorModal = document.getElementById('editsensorModal');
        editsensorModal.addEventListener('show.bs.modal', function (event) {
          // Button that triggered the modal
          var button = event.relatedTarget;
          // Extract info from data-bs-* attributes
          var recipient = button.getAttribute('data-bs-whatever');
          // If necessary, you could initiate an AJAX request here
          // and then do the updating in a callback.
          //
          // Update the modal's content.
          var modalTitle = editsensorModal.querySelector('.modal-title');
          var modalBodyInput = editsensorModal.querySelector('.modal-body input');

          modalTitle.textContent = 'Edit sensor ';
          modalBodyInput.value = recipient;
        });
      }
    };
    xhttp.open("GET", "sensor_ajax.php?data_id=" + data_id, true);
    xhttp.send();
  }