/*
  Serial Event 

  When new serial data arrives, this sketch adds it to a String.
  When a newline is received, the loop prints the string and clears it.

*/
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
int resval = 0;  // holds the value
int respin = A5; // sensor pin used

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

String inputString = "";         // a String to hold incoming data
String url = "AT+HTTPPARA=\"URL\",\"http://aquagauge.co.ke/aqua_gauge2/insert2.php?sensor_id=4&level=";
String url2 = "\"\r";
bool stringComplete = false;  // whether the string is complete

void setup() {
  // initialize serial:
  Serial.begin(9600);
  lcd.init();// initialize the lcd 
  lcd.init();// Print a message to the LCD.
  lcd.backlight();
  
  Serial1.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
}

void loop() {
  delay(5000);
  resval = analogRead(respin); //Read data from analog pin and store it to resval variable
 
  lcd.clear(); 
  if (resval<=20)
  { 
    Serial.println("Water Level: Empty"); 
    lcd.setCursor(1,1);
  lcd.print("EMPTY");
  } 
  else if (resval>20 && resval<=50)
  { 
    Serial.println("Water Level: Low"); 
    lcd.setCursor(1,1);
  lcd.print("LOW");
  } 
  else if (resval>50 && resval<=150)
  { 
    Serial.println("Water Level: Medium"); 
    lcd.setCursor(1,1);
  lcd.print("MEDIUM");
  } 
  else if (resval>150)
  { 
    Serial.println("Water Level: High"); 
    lcd.setCursor(1,1);
  lcd.print("HIGH");
  }
  Serial.print("level: ");
  Serial.println(resval);
   lcd.setCursor(3,0);
  lcd.print(resval);
 
  
  // print the string when a newline arrives:
  //Serial.print("ATE1->");
  while(1){
    Serial1.write("ATE1\r");
    delay(2000);
    String mstr=inputString;
    char c[20];
    mstr.toCharArray(c, sizeof(c));
    Serial.write(c);
    delay(500);
    
    if(mstr.indexOf("OK")){
    inputString = "";
    Serial.print(mstr);
    Serial.println("ATE0 complete");
    break;
    }
  }
    //Serial.print("AT-->");
    Serial1.print("AT\r");
    delay(2000);
    //Serial.println(inputString);
    //delay(500);
    inputString = "";
    //Serial.print("AT+SAPBR=3,1,\"Contype\",\"GPRS\"-->");
    Serial1.print("AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r");
    delay(2000);
    Serial.println(inputString);
    delay(200);
    inputString = "";
    //Serial.print("AT+SAPBR=3,1,\"APN\",\"safaricom\"-->");
    Serial1.print("AT+SAPBR=3,1,\"APN\",\"safaricom\"\r");
    delay(1000);
    //Serial.println(inputString);
    // clear the string:
    inputString = "";
    Serial.print("AT+SAPBR=3,1,\"USER\",\"saf\"-->");
    Serial1.print("AT+SAPBR=3,1,\"USER\",\"saf\"\r");
    delay(1000);
    //Serial.println(inputString);
    // clear the string:
    inputString = ""; 
    Serial.print("AT+SAPBR=3,1,\"PWD\",\"data\"-->");
    Serial1.print("AT+SAPBR=3,1,\"PWD\",\"data\"\r");
    delay(1000);
    //Serial.println(inputString);
    // clear the string:
    inputString = "";      
    Serial.print("AT+SAPBR=1,1-->");
    Serial1.print("AT+SAPBR=1,1\r");
    delay(3000);
    //Serial.println(inputString);
    // clear the string:
    inputString = "";  
    Serial.print("AT+SAPBR=2,1-->");
    Serial1.print("AT+SAPBR=2,1\r");
    delay(1000);
    //Serial.println(inputString);
    // clear the string:
    inputString = "";    
    Serial.print("AT+HTTPINIT-->");
    Serial1.print("AT+HTTPINIT\r");
    delay(1000);
    //Serial.println(inputString);
    // clear the string:
    inputString = "";  
    Serial.print("AT+HTTPPARA=\"CID\",1-->");
    Serial1.print("AT+HTTPPARA=\"CID\",1\r");
    delay(1000);
    //Serial.println(inputString);
    // clear the string:
    inputString = "";  
    String url3 = url+resval+url2 ;
    Serial.println(url3);
    Serial1.println(url3);
    //Serial.print("AT+HTTPPARA=\"URL\",\"http://aquagauge.co.ke/aquagauge/gsm/php/insert_level.php?level=71\"-->");
    //Serial1.print("AT+HTTPPARA=\"URL\",\"http://aquagauge.co.ke/aquagauge/gsm/php/insert_level.php?level=71\"\r");
    delay(1000);
    //Serial.println(inputString);
    // clear the string:
    inputString = "";
    Serial.print("AT+HTTPACTION=0-->");
    Serial1.print("AT+HTTPACTION=0\r");
    delay(1000);
    //Serial.println(inputString);
    // clear the string:
    inputString = "";           
    Serial.print("AT+HTTPREAD-->");
    Serial1.print("AT+HTTPREAD\r");
    delay(1000);
    Serial.println(inputString);
    //clear the string:
    inputString = "";  
                                                                                                                         
  
}

/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/
void serialEvent1() {
  while (Serial1.available()) {
    // get the new byte:
    char inChar = (char)Serial1.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}