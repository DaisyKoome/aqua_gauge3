function get_waterlevel_details(water_level) {
 
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {

        var waterObject = JSON.parse(this.responseText);
        console.log(this.responseText);
        document.getElementById("inside_div").value = waterObject.level;

      }
    };
    xhttp.open("GET", "tank.php?water_level=" + water_level, true);
    xhttp.send();
  }