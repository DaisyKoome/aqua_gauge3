<!DOCTYPE html>
<?php
session_start();
if(!isset($_SESSION['adminlogin'])){
    header("Location: clientlogin.php");
}
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="dist/img/logo.png">
  <title>Admin Dashboard - Sensors | Aqua_Gauge</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

      
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    
    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/5a4db65dc4.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="sensor_script.js"></script>
    <script type="text/javascript" src="sensor_delete.js"></script>
    

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/album/album.css" rel="stylesheet">

    <!-- Own CSS -->
    <link rel="stylesheet" type="text/css" href="../css/gui_index.css">
    <link rel="stylesheet" href="../css/water_animation.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed" style="font-family: Quicksand ;">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="dist/img/logo.png" alt="Logo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index_sensors.php" class="nav-link">Sensors</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index_clients.php" class="nav-link">Clients</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index_vendors.php" class="nav-link">Vendors</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#contacts" class="nav-link">Contacts</a>
      </li>
      <button type="button" class="btn btn-outline-danger" style="padding:0px;"> 
            <a href="admin_logout.php" class="nav-link" style="text-decoration:none; color:black;">Log out</a>
      </button>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/logo.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Aqua_Gauge</span> <br>
    </a>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Admin Dashboard
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="index_sensors.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sensors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index_clients.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Clients</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index_vendors.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Vendors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#contacts" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                  <p>Contacts</p>
                </a>
              </li>
            </ul>
          </li>  
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"style="padding-top:0px;">
    <!-- Content Header (Page header) -->
    <div class="content-header" style="padding-top:0px;padding-bottom:0px;">
      <div class="container-fluid" style="padding-top:0px; margin-top:0px;padding-bottom:0px;">
        <div class="row mb-2" style="padding-top:0px; margin-top:0px;padding-bottom:0px;">
          <div class="col-sm-6" style="padding:5px;">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" style="padding-top:0px; margin-top:0px;">
      <div class="container-fluid">

<!-- Sensors -->  

  <div class="row pe-lg-0 pt-lg-5 align-items-center rounded-3 border shadow-lg" style="padding-top:0px; margin-top:0px;">
    <div class="col-lg-12 p-lg-10 pt-lg-8" style="padding-top:0px; margin-top:0px;">
      <h1 class="display-6 fw-bold text-center" id="clients" style="padding-top:0px; margin-top:0px;">Sensors</h1>
      <nav class="navbar navbar-light bg-light d-flex justify-content-center col-11 col-lg-8 ml-auto mr-auto" style="padding-top:0px; margin-top:0px;">
          <div class="container-fluid " style="padding-top:0px; margin-top:0px;">
            <form class="display-6 fw-bold  text-center col-11 col-lg-8 ml-auto mr-auto">
              <input class="form-control me-2 col-11 col-lg-8 ml-auto mr-auto" type="search" id="sensorSearch" onkeyup="sensorFunc()" placeholder="Search" aria-label="Search">
              <button type="button" class="btn btn-outline-info col-4 col-lg-3 ml-auto mr-auto" data-bs-toggle="modal" data-bs-target="#sensorModal">
              <i class="fas fa-plus"></i>
              </button>
              <br>
              <?php
                if(isset($_SESSION['errors'])){
                    echo "<div class='alert alert-danger'>";
                    for ($i=0; $i < sizeof($_SESSION['errors']); $i++) { 
                        echo "*".$_SESSION['errors'][$i]."<br>";
                    }
                    echo '</div>';
                    unset($_SESSION['errors']);
                }
              ?>
            </form>
           </div> 
      </nav>
    </div>
<!-- Sensors table -->
<div class="container d-flex justify-content-center" style="margin-bottom: 50px;">
<div class="table-responsive table-striped table-bordered" style='width: 900px;'>
  <table class="table" id="sensorTable">
     
      <?php
        // include database connection
        include 'db2.php';
          // select all data

          $query = "SELECT * FROM data";
          $results = mysqli_query($db, $query);
          $num = mysqli_num_rows($results);
          //check if more than 0 record found
          if($num >= 1){
           
              // data from database will be here
              echo "<tr class='text-center'>";
                  echo "<th scope='col'>Data ID</th>";
                  echo "<th scope='col'>Sensor ID</th>";
                  echo "<th scope='col'>Water level</th>";
                  echo "<th scope='col'>Time</th>";
                  echo "<th scope='col'></th>";
                  echo "<th scope='col'></th>";
              echo "</tr>";
   

            }
            while ($row=mysqli_fetch_array($results)) {
                  echo "<tr class='text-center'>";
                      echo "<td>".$row['data_id']."</td>";
                      echo "<td>".$row['sensor_id']."</td>";
                      echo "<td>".$row['water_level']."</td>";
                      echo "<td>".$row['time']."</td>";
                      echo "<td>
                      <button onclick=get_waterlevel_details(\"".$row['data_id']."\") data-bs-toggle='modal' data-bs-target='#editsensorModal' name='sensor_edit' class='btn btn-primary'>
                      Edit
                      </button>
                      </td>";
                      echo "<td>
                      <button onclick=delete_sensor_details(\"".$row['data_id']."\") class='btn btn-danger'> 
                      Delete
                      </button>
                      </td>";                      
                      echo "</tr>";
              }    
                      
?>                                     
  </table>
</div>
</div>
</div>
<!-- End of Sensors table -->

</main>

<!-- Add Sensor Modal -->
<div class="modal fade" id="sensorModal" tabindex="-1" aria-labelledby="sensorModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="sensorModalLabel"></h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
    <form  method="post" action="sensor_add.php">
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Sensor ID</label>
        <input type="text" class="form-control" name="d_id2" id="d_id2" placeholder="Enter sensor ID">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Water level</label>
        <input type="text" class="form-control" name="wlevel" id="wlevel" placeholder="Enter water level">
    </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      <button type="submit" name="sensor_add" class="btn btn-primary">Save changes</button>
    </div>
    </form>
  </div>
</div>
</div>
<!-- End of Add sensor Modal -->

<!-- Edit sensor Modal -->
<div class="modal fade" id="editsensorModal" tabindex="-1" aria-labelledby="editsensorModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editsensorModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
    <div class="modal-body">
    <form action="admin_sensor_edit.php" method="POST">
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
    <label for="exampleFormControlInput1">Data ID</label>
    <input type="text" class="form-control" name="data_id" id="data_id" placeholder="Enter data ID">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Sensor ID</label>
        <input type="text" class="form-control" name="d_id4" id="d_id4" placeholder="Enter sensor ID">
        <input type="hidden" name="dev_id4" id="dev_id4">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Water level</label>
        <input type="text" class="form-control" name="wlevel2" id="wlevel2" placeholder="Enter water level">
    </div>           
    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" name="sensor_edit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<!-- End of Edit sensor Modal -->

<script>
//Display sensor modal
var sensorModal = document.getElementById('sensorModal')
      sensorModal.addEventListener('show.bs.modal', function (event) {
        // Button that triggered the modal
        var button = event.relatedTarget
        // Extract info from data-bs-* attributes
        var recipient = button.getAttribute('data-bs-whatever')
        // If necessary, you could initiate an AJAX request here
        // and then do the updating in a callback.
        //
        // Update the modal's content.
        var modalTitle = sensorModal.querySelector('.modal-title')
        var modalBodyInput = sensorModal.querySelector('.modal-body input')

        modalTitle.textContent = 'Add a sensor '
        modalBodyInput.value = recipient
      })

function sensorFunc() {
var input, filter, table, tr, td, i, txtValue;
var j=0;
input = document.getElementById("sensorSearch");
filter = input.value.toUpperCase();
table = document.getElementById("sensorTable");
tr = table.getElementsByTagName("tr");
for (i = 0; i < tr.length; i++) {
  
  for(j=0;j<3;j++){
  td = tr[i].getElementsByTagName("td")[j];
  if (td) {
    txtValue = td.textContent || td.innerText;
    
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      tr[i].style.display = "";
      break;
    } else {
      tr[i].style.display = "none";
    }
  }  
  }     
}
}

</script>

<!-- Footer -->
<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" style="background-color: #212529;">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner" id="contacts">
    <div class="carousel-item active">
      <img src="img/contacts.png" width="180px" height="120px" class="d-block w-20" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h4>Call/SMS this number for assistance:</h4>
        <h5>0799448090</h5>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/gmail.png" width="180px" height="120px" class="d-block w-20" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h4>Send an email to:</h4>
        <h5>dskoome2@gmail.com</h5>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/twitter.png" width="180px" height="120px" class="d-block w-20" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h4>Twitter handle:</h4>
        <h5>@aqua_gauge</h5>
      </div>
    </div>
  </div>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>
     
  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
  <script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"><\/script>')</script>
  <!--<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
  <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
  <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>-->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>-->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
</body>
</html>
