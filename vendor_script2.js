function get_vendor_details(vendor_id) {
 
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var myObject = JSON.parse(this.responseText);
       
        //alert (this.responseText);
    
        //console.log (this.responseText);
        document.getElementById("vendor_id").value = myObject.vendor_id;     
        document.getElementById("vn_id").value = myObject.vn_id;
        //console.log(myObject.vendorid);
        document.getElementById("vfname").value = myObject.vfname;
        document.getElementById("vlname").value = myObject.vlname;
        document.getElementById("vemail").value = myObject.vemail;
        document.getElementById("vpno").value = myObject.vpno;
        document.getElementById("vlocation").value = myObject.vlocation;


        //Display modal
       
        var vendoreditModal = document.getElementById('vendoreditModal');
        vendoreditModal.addEventListener('show.bs.modal', function (event) {
          // Button that triggered the modal
          var button = event.relatedTarget;
          // Extract info from data-bs-* attributes
          var recipient = button.getAttribute('data-bs-whatever');
          // If necessary, you could initiate an AJAX request here
          // and then do the updating in a callback.
          //
          // Update the modal's content.
          var modalTitle = vendoreditModal.querySelector('.modal-title');
          var modalBodyInput = vendoreditModal.querySelector('.modal-body input');

          modalTitle.textContent = 'Edit vendor  ';
          modalBodyInput.value = recipient;
        });
      }
    };
    xhttp.open("GET", "vendor_ajax2.php?vendor_id=" + vendor_id, true);
    xhttp.send();
  }