<?php
require "pdfcrowd.php";

try
{
    // create the API client instance
    $client = new \Pdfcrowd\HtmlToImageClient("dee2", "700a3493fa86390249e62589471c3482");

    // configure the conversion
    $client->setOutputFormat("png");

    // run the conversion and write the result to a file
    $client->convertUrlToFile("http://localhost/aqua_gauge3/trial2.php", "example.png");
}
catch(\Pdfcrowd\Error $why)
{
    // report the error
    error_log("Pdfcrowd Error: {$why}\n");

    // rethrow or handle the exception
    throw $why;
}

?>