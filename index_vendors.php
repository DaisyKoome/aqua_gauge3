<!DOCTYPE html>
<html lang="en">
<?php
include "controller/admin.php";
include "views/header.html";

session_start();
if(!isset($_SESSION['adminlogin'])){
    header("Location: clientlogin.php");
}
?>
<body class="hold-transition sidebar-mini layout-fixed" style="font-family: Quicksand ;">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="dist/img/logo.png" alt="Logo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index_sensors.php" class="nav-link">Sensors</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index_clients.php" class="nav-link">Clients</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index_vendors.php" class="nav-link">Vendors</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#contacts" class="nav-link">Contacts</a>
      </li>
      <button type="button" class="btn btn-outline-danger" style="padding:0px;"> 
            <a href="admin_logout.php" class="nav-link" style="text-decoration:none; color:black;">Log out</a>
      </button>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/logo.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Aqua_Gauge</span> <br>
    </a>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Admin Dashboard
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="index_sensors.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sensors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index_clients.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Clients</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="index_vendors.php" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Vendors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#contacts" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                  <p>Contacts</p>
                </a>
              </li>               
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"style="padding-top:0px;">
    <!-- Content Header (Page header) -->
    <div class="content-header" style="padding-top:0px;padding-bottom:0px;">
      <div class="container-fluid" style="padding-top:0px; margin-top:0px;padding-bottom:0px;">
        <div class="row mb-2" style="padding-top:0px; margin-top:0px;padding-bottom:0px;">
          <div class="col-sm-6" style="padding:5px;">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" style="padding-top:0px; margin-top:0px;">
      <div class="container-fluid">

<!-- Vendors -->  

  <div class="row pe-lg-0 pt-lg-5 align-items-center rounded-3 border shadow-lg" style="padding-top:0px; margin-top:0px;">
    <div class="col-lg-12 p-lg-10 pt-lg-8" style="padding-top:0px; margin-top:0px;">
      <h1 class="display-6 fw-bold text-center" id="clients" style="padding-top:0px; margin-top:0px;">Vendors</h1>
      <nav class="navbar navbar-light bg-light d-flex justify-content-center col-11 col-lg-8 ml-auto mr-auto" style="padding-top:0px; margin-top:0px;">
          <div class="container-fluid " style="padding-top:0px; margin-top:0px;">
          <form class="display-6 fw-bold  text-center col-11 col-lg-8 ml-auto mr-auto">
          <input class="form-control me-2 col-11 col-lg-8 ml-auto mr-auto" type="search" id="sensorSearch" onkeyup="sensorFunc()" placeholder="Search" aria-label="Search">
           <!-- <button class="btn btn-outline-info col-4 col-lg-3 ml-auto mr-auto" type="submit">Search</button>-->
           <button type="button" class="btn btn-outline-info col-4 col-lg-3 ml-auto mr-auto" data-bs-toggle="modal" data-bs-target="#addvendorModal">
              <i class="fas fa-plus"></i>
            </button>
          </form>
        </div>  
    </nav>
  </div>

<!-- Vendors table -->
<div class="container d-flex justify-content-center" style="margin-bottom: 50px;">
<div class="table-responsive table-striped table-bordered" style='width: 900px;'>
  <table class="table" id="vendorTable">
     
      <?php
        // include database connection
        include 'db2.php';
          // select all data

          $query = "SELECT * FROM vendor";
          $results = mysqli_query($db, $query);
          $num = mysqli_num_rows($results);
          //check if more than 0 record found
          if($num >= 1){
           
              // data from database will be here
              echo "<tr class='text-center'>";
                  echo "<th scope='col'>Vendor ID</th>";
                  echo "<th scope='col'>National ID</th>";
                  echo "<th scope='col'>First name</th>";
                  echo "<th scope='col'>Last name</th>";
                  echo "<th scope='col'>Email</th>";
                  echo "<th scope='col'>Phone number</th>";
                  echo "<th scope='col'>Status</th>";
                  echo "<th scope='col'>Location</th>";
                  echo "<th scope='col'></th>";
                  echo "<th scope='col'></th>";                  
              echo "</tr>";
   

            }
            while ($row=mysqli_fetch_array($results)) {
                  echo "<tr class='text-center'>";
                      echo "<td>".$row['vendor_id']."</td>";
                      echo "<td>".$row['national_id']."</td>";
                      echo "<td>".$row['first_name']."</td>";
                      echo "<td>".$row['last_name']."</td>";
                      echo "<td>".$row['email']."</td>";
                      echo "<td>".$row['phone_no']."</td>";
                      echo "<td>
                      <div class='alert ";
                      if($row['status']=="Approved") {echo "alert-success "; $status="Approved";}
                      else if($row['status']=="Denied") {echo "alert-danger "; $status="Denied";}
                      else  {echo "alert-info "; $status="Pending";}
                       
                       echo "text-center' role='alert' style='margin-left: 0px; margin-right: 0px; margin-bottom: 0px; padding-left: 5px; padding-right: 5px;'>".$status."</div>
                      </td>";                        
                      echo "<td>".$row['location']."</td>";
                      echo "<td>
                      <button onclick=get_vendor_details(\"".$row['vendor_id']."\") data-bs-toggle='modal' data-bs-target='#vendoreditModal' name='edit_vendor' class='btn btn-primary'>
                      Edit
                      </button>
                      </td>";
                      echo "<td>
                      <button onclick=delete_vendor_details(\"".$row['vendor_id']."\") class='btn btn-danger'> 
                      Delete
                      </button>
                      </td>";                      
                      echo "</tr>";
              }    
   
?>                                     
  </table>
</div>
</div>
</div>
<!-- End of vendor table -->

</main>

<!-- Add vendor Modal -->
<div class="modal fade" id="addvendorModal" tabindex="-1" aria-labelledby="addvendorModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="addvendorModalLabel"></h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
    <form action="vendor_add.php" method="POST">
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">National ID</label>
        <input type="text" class="form-control" name="n_id3" id="n_id3" placeholder="Enter vendor's National ID">
    </div>    
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">First name</label>
        <input type="text" class="form-control" name="first_name3" id="fname3" placeholder="Enter vendor's first name">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Last name</label>
        <input type="text" class="form-control" name="last_name3" id="lname3" placeholder="Enter vendor's last name">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Email</label>
        <input type="text" class="form-control" name="email3" id="email3" placeholder="Enter vendor's email address">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Password</label>
        <input type="password" class="form-control" name="password3" id="password3" placeholder="Enter password">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Phone number</label>
        <input type="text" class="form-control" name="phone_no3" id="phone_no3" placeholder="Enter vendor's phone number">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Location</label>
        <select name='location3' class="form-select" id="location3">
            <option value="Kitengela">Kitengela</option>
            <option value="Isinya">Isinya</option>
            <option value="Kajiado town">Kajiado town</option>
        </select>
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Status</label>
        <select name='status3' class="form-select" id="status3">
            <option value="Approved">Approved</option>
            <option value="Denied">Denied</option>
            <option value="Pending">Pending</option>
        </select>
    </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      <button type="submit" name="vendor_add" class="btn btn-primary">Save changes</button>
    </div>
    </form>
  </div>
</div>
</div>
<!-- End of Add vendor Modal -->

<!-- Vendor Edit Modal -->
<div class="modal fade" id="vendoreditModal" tabindex="-1" aria-labelledby="vendoreditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="vendoreditModalLabel">Modal title</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
    <div class="modal-body">
    <form action="admin_vendor_edit.php" method="POST">
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">National ID</label>
        <input type="text" class="form-control" name="vn_id" id="vn_id" placeholder="Enter your first name">
        <input name="vendor_id" type="hidden" id="vendor_id">        
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">First name</label>
        <input type="text" class="form-control" name="vfname" id="vfname" placeholder="Enter your first name">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Last name</label>
        <input type="text" class="form-control" name="vlname" id="vlname" placeholder="Enter your last name">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Email</label>
        <input type="email" class="form-control" name="vemail" id="vemail" placeholder="Enter your email">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Phone number</label>
        <input type="text" class="form-control" name="vpno" id="vpno" placeholder="Enter your phone number">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Location</label>
        <select name='vlocation' class="form-select" id="vlocation">
            <option value="Kitengela">Kitengela</option>
            <option value="Isinya">Isinya</option>
            <option value="Kajiado town">Kajiado town</option>
        </select>
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Status</label>
        <select name='vstatus' class="form-select" id="vstatus">
            <option value="Approved">Approved</option>
            <option value="Denied">Denied</option>
            <option value="Pending">Pending</option>
        </select>
    </div>               
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" name="edit_vendor" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<!-- End of Vendor Edit Modal -->

<script>

var addvendorModal = document.getElementById('addvendorModal')
addvendorModal.addEventListener('show.bs.modal', function (event) {
        // Button that triggered the modal
        var button = event.relatedTarget
        // Extract info from data-bs-* attributes
        var recipient = button.getAttribute('data-bs-whatever')
        // If necessary, you could initiate an AJAX request here
        // and then do the updating in a callback.
        //
        // Update the modal's content.
        var modalTitle = addvendorModal.querySelector('.modal-title')
        var modalBodyInput = addvendorModal.querySelector('.modal-body input')

        modalTitle.textContent = 'Add vendor '
        modalBodyInput.value = recipient
      })

      function sensorFunc() {
var input, filter, table, tr, td, i, txtValue;
var j=0;
input = document.getElementById("sensorSearch");
filter = input.value.toUpperCase();
table = document.getElementById("sensorTable");
tr = table.getElementsByTagName("tr");
for (i = 0; i < tr.length; i++) {
  
  for(j=0;j<7;j++){
  td = tr[i].getElementsByTagName("td")[j];
  if (td) {
    txtValue = td.textContent || td.innerText;
    
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      tr[i].style.display = "";
      break;
    } else {
      tr[i].style.display = "none";
    }
  }  
  }     
}
}
</script>

<!-- Footer -->
<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" style="background-color: #212529;">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner" id="contacts">
    <div class="carousel-item active">
      <img src="img/contacts.png" width="180px" height="120px" class="d-block w-20" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h4>Call/SMS this number for assistance:</h4>
        <h5>0799448090</h5>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/gmail.png" width="180px" height="120px" class="d-block w-20" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h4>Send an email to:</h4>
        <h5>dskoome2@gmail.com</h5>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/twitter.png" width="180px" height="120px" class="d-block w-20" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h4>Twitter handle:</h4>
        <h5>@aqua_gauge</h5>
      </div>
    </div>
  </div>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>
     
  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
  <script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"><\/script>')</script>
  <!--<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
  <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
  <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>-->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>-->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
</body>
</html>
