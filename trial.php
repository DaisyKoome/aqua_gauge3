<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../img/logo.png">
    
        <title>Admin - Aqua_Gauge</title>
    
        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    
        <!-- Bootstrap core CSS -->
        <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://kit.fontawesome.com/5a4db65dc4.js" crossorigin="anonymous"></script>
        <script type="text/javascript" src="client_script.js"></script>
        <script type="text/javascript" src="vendor_script.js"></script>
        
    
        <!-- Custom styles for this template -->
        <link href="https://getbootstrap.com/docs/4.0/examples/album/album.css" rel="stylesheet">
    
        <!-- Own CSS -->
        <link rel="stylesheet" type="text/css" href="../css/gui_index.css">
        <link rel="stylesheet" href="../css/water_animation.css">
    
        <style type="text/css">
          #chart-container {
              width: 640px;
              height: 500px;
          }
          .bs-example{
            margin: 20px;
              position: relative;
          }
        </style>
        <script>
            $(document).ready(function(){
                $(".show-toast").click(function(){
                    $("#myToast").toast('show');
                });
            });
        </script> 
    </head>
    <body>
<main>
  <!-- Navbar -->
  <nav class="navbar navbar-dark bg-dark">
    <a href="/" class="d-flex align-items-center">
      <img src="../img/logo.png" width="80" height="64" alt="">
    </a>
    <table>
        <tr>
          <td>
            <h1 class="text-center" style="color: #007cba;">AQUA_GAUGE</h1>
          </td>
        </tr>
      <tr>
        <td>
          <h4 class="text-center" style="color: #98cce6;">Water is life.</h4>
        </td>
      </tr>
    </table>
    <table>
      <tr>
        <td>
          <button type="button" class="show-toast btn btn-dark">
            <i class="fa fa-bell"></i>
          </button>
        </td>
        <td>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </td>
      </tr>
    </table>

    <div class="collapse navbar-collapse" id="navbarsExample01">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#clients"> Clients <span class="sr-only">(current)</span></a>
          <a class="nav-link" href="#vendors"> Vendors <span class="sr-only">(current)</span></a>
          <a class="nav-link" href="#contacts"> Contacts <span class="sr-only">(current)</span></a>
        </li>

    </div>
</nav>

  <!-- Main body - Client's page -->
  <div class="bs-example">  
    <div class="toast" id="myToast" style="position: absolute; top: 0; right: 0;">
        <div class="toast-header">
            <small>11 mins ago</small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close">
                
            </button>
        </div>
        <div class="toast-body">
            <div>It's been a long time since you visited us. We've something special for you. <a href="#">Click here!</a></div>
        </div>
    </div>
</div>

<main role="main">
<!-- Clients -->  
  <div class="container my-5">
    <div class="row p-4 pb-0 pe-lg-0 pt-lg-5 align-items-center rounded-3 border shadow-lg">
      <div class="col-lg-12 p-3 p-lg-10 pt-lg-8">
        <h1 class="display-6 fw-bold text-center" id="clients">Clients</h1>
        <nav class="navbar navbar-light bg-light d-flex justify-content-center col-11 col-lg-8 ml-auto mr-auto">
            <div class="container-fluid ">
              <form class="display-6 fw-bold  text-center col-11 col-lg-8 ml-auto mr-auto">
                <input class="form-control me-2 col-11 col-lg-8 ml-auto mr-auto" type="search" id="clientSearch" onkeyup="clientFunc()" placeholder="Search by Client ID" aria-label="Search">
                <!--<button class="btn btn-outline-info col-4 col-lg-3 ml-auto mr-auto" type="submit">Search</button>-->
                </form>
        </nav>
      </div>

<!-- Clients table -->
<div class="container" style="margin-bottom: 50px;">
  <div class="table-responsive table-striped table-bordered">
    <table class="table" id="clientTable">
       
        <?php
          // include database connection
          include 'db2.php';
            // select all data

            $query = "SELECT * FROM client";
            $results = mysqli_query($db, $query);
            $num = mysqli_num_rows($results);
            //check if more than 0 record found
            if($num >= 1){
             
                // data from database will be here
                echo "<tr>";
                    echo "<th scope='col'>Client ID</th>";
                    echo "<th scope='col'>Device ID</th>";
                    echo "<th scope='col'>First name</th>";
                    echo "<th scope='col'>Last name</th>";
                    echo "<th scope='col'>Tank status</th>";
                    echo "<th scope='col'>Email</th>";
                    echo "<th scope='col'>Phone number</th>";
                    echo "<th scope='col'>Status</th>";
                    echo "<th scope='col'>Vendor</th>";
                    echo "<th scope='col'>Location</th>";
                echo "</tr>";
     

              }
              while ($row=mysqli_fetch_array($results)) {
                    echo "<tr>";
                        echo "<td>".$row['client_id']."</td>";
                        echo "<td>".$row['device_id']."</td>";
                        echo "<td>".$row['first_name']."</td>";
                        echo "<td>".$row['last_name']."</td>";
                        echo "<td>
                        <div class='progress'>
                        <div class='progress-bar bg-info' role='progressbar' style='width: 50%; color:black;' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'>50%</div>
                        <div class='progress-bar bg-dark' role='progressbar' style='width: 50%; color:#98cce6;' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'></div>
                        </div>            
                        </td>";
                        echo "<td>".$row['email']."</td>";
                        echo "<td>".$row['phone_no']."</td>";
                        echo "<td>
                        <div class='alert ";
                        if($row['status']=="Approved") {echo "alert-success "; $status="Approved";}
                        else if($row['status']=="Denied") {echo "alert-danger "; $status="Denied";}
                        else  {echo "alert-info "; $status="Pending";}
                         
                         echo "text-center' role='alert' style='margin-left: 0px; margin-right: 0px; margin-bottom: 0px; padding-left: 5px; padding-right: 5px;'>".$status."</div>
                        </td>";                        
                        echo "<td>".$row['vendor_id']."</td>";                        
                        echo "<td>".$row['location']."</td>";
                        echo "<td>
                        <button onclick=get_client_details(\"".$row['client_id']."\") data-bs-toggle='modal' data-bs-target='#exampleModal' name='edit_cust' class='btn btn-primary'>
                        Edit
                        </button>
                        </td>";
                        echo "<td>
                        <button class='btn btn-danger text-center ml-auto mr-auto'>
                        <a href='admin_delete.php' style='text-decoration:none; color:white;'>Delete</a>
                        </button>
                        </td>";
                        echo "</tr>";
                }    
                        
  ?>                    
  
            //check if more than 0 record found
            if($num >= 1){
             
                // data from database will be here
                echo "<tr>";
                    echo "<th scope='col'>Vendor ID</th>";
                    echo "<th scope='col'>First name</th>";
                    echo "<th scope='col'>Last name</th>";
                    echo "<th scope='col'>Email</th>";
                    echo "<th scope='col'>Phone number</th>";
                    echo "<th scope='col'>Status</th>";
                    echo "<th scope='col'>Location</th>";
                echo "</tr>";
     

              }
              while ($row=mysqli_fetch_array($results)) {
                    echo "<tr>";
                        echo "<td>".$row['vendor_id']."</td>";
                        echo "<td>".$row['first_name']."</td>";
                        echo "<td>".$row['last_name']."</td>";
                        echo "<td>".$row['email']."</td>";
                        echo "<td>".$row['phone_no']."</td>";
                        echo "<td>
                        <div class='alert ";
                        if($row['status']=="Approved") {echo "alert-success "; $status="Approved";}
                        else if($row['status']=="Denied") {echo "alert-danger "; $status="Denied";}
                        else  {echo "alert-info "; $status="Pending";}
                         
                         echo "text-center' role='alert' style='margin-left: 0px; margin-right: 0px; margin-bottom: 0px; padding-left: 5px; padding-right: 5px;'>".$status."</div>
                        </td>";                        
                        echo "<td>".$row['location']."</td>";
                        echo "<td>
                        <button onclick=get_vendor_details(\"".$row['vendor_id']."\") data-bs-toggle='modal' data-bs-target='#vendoreditModal' name='edit_vendor' class='btn btn-primary'>
                        Edit
                        </button>
                        </td>";
                        echo "<td>
                        <button class='btn btn-danger text-center ml-auto mr-auto'>
                        <a href='admin_delete.php' style='text-decoration:none; color:white;'>Delete</a>
                        </button>
                        </td>";
                        echo "</tr>";
                }    
     
  ?>                                     
    </table>
  </div>
</div>
<!-- End of vendor table -->
</main>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
      <div class="modal-body">
      <form action="admin_client_edit.php" method="POST">
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Client ID</label>
          <input type="text" class="form-control" name="c_id" id="c_id" placeholder="Enter your first name">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">First name</label>
          <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter your first name">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Last name</label>
          <input type="text" class="form-control" name="lname" id="lname" placeholder="Enter your last name">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Email</label>
          <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Phone number</label>
          <input type="text" class="form-control" name="pno" id="pno" placeholder="Enter your phone number">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Location</label>
          <input type="text" class="form-control" name="location" id="location" placeholder="Enter your location">
          <input name="cust_id" type="hidden" id="cust_id">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Status</label>
          <select name='status' class="form-select" id="status">
              <option value="Approved">Approved</option>
              <option value="Denied">Denied</option>
              <option value="Pending">Pending</option>
          </select>
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Vendor ID</label>
          <select name='vendor' class="form-select" id="vendor_id">
          <?php
              // include database connection
              include 'db2.php';
              $query = "SELECT * FROM vendor";
              $results = mysqli_query($db, $query);
              while ($row=mysqli_fetch_array($results)) {
                echo "<option value='".$row['vendor_id']."'>".$row['first_name'].$row['last_name']."</option>";
              }
          ?>  
          </select>
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Device ID</label>
          <select name='device' class="form-select" id="device_id">
          <?php
              // include database connection
              include 'db2.php';
              $query = "SELECT * FROM device";
              $results = mysqli_query($db, $query);
              while ($row=mysqli_fetch_array($results)) {
                echo "<option value='".$row['device_id']."'>".$row['device_id']."</option>";
              }
          ?>  
          </select>                            
      </div>                     
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
          </form>
        </div>
      </div>
    </div>
<!-- End of Vendor Edit Modal -->

<!-- Vendor Edit Modal -->
<div class="modal fade" id="vendoreditModal" tabindex="-1" aria-labelledby="vendoreditModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="vendoreditModalLabel">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
      <div class="modal-body">
      <form action="admin_vendor_edit.php" method="POST">
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Vendor ID</label>
          <input type="text" class="form-control" name="v_id" id="v_id" placeholder="Enter your first name">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">First name</label>
          <input type="text" class="form-control" name="vfname" id="vfname" placeholder="Enter your first name">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Last name</label>
          <input type="text" class="form-control" name="vlname" id="vlname" placeholder="Enter your last name">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Email</label>
          <input type="email" class="form-control" name="vemail" id="vemail" placeholder="Enter your email">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Phone number</label>
          <input type="text" class="form-control" name="vpno" id="vpno" placeholder="Enter your phone number">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Location</label>
          <input type="text" class="form-control" name="vlocation" id="vlocation" placeholder="Enter your location">
          <input name="ven_id" type="hidden" id="ven_id">
      </div>
      <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
          <label for="exampleFormControlInput1">Status</label>
          <select name='vstatus' class="form-select" id="vstatus">
              <option value="Approved">Approved</option>
              <option value="Denied">Denied</option>
              <option value="Pending">Pending</option>
          </select>
      </div>               
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
          </form>
        </div>
      </div>
    </div>
<!-- End of Vendor Edit Modal -->

<script>
function clientFunc() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("clientSearch");
  filter = input.value.toUpperCase();
  table = document.getElementById("clientTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}

function vendorFunc() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("vendorSearch");
  filter = input.value.toUpperCase();
  table = document.getElementById("vendorTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}


</script>

  <!-- Footer -->
  <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" style="background-color: #212529;">
    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner" id="contacts">
      <div class="carousel-item active">
        <img src="../img/contacts.png" width="180px" height="120px" class="d-block w-20" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h4>Call/SMS this number for assistance:</h4>
          <h5>0799448090</h5>
        </div>
      </div>
      <div class="carousel-item">
        <img src="../img/gmail.png" width="180px" height="120px" class="d-block w-20" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h4>Send an email to:</h4>
          <h5>dskoome2@gmail.com</h5>
        </div>
      </div>
      <div class="carousel-item">
        <img src="../img/twitter.png" width="180px" height="120px" class="d-block w-20" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h4>Twitter handle:</h4>
          <h5>@aqua_gauge</h5>
        </div>
      </div>
    </div>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>
       
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
    <script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"><\/script>')</script>
    <!--<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>-->
    


    <!--Own Javascript files
    <script type="text/javascript" src="script.js"></script>-->
    <script type="text/javascript" src="../chartjs/js/jquery.min.js"></script>
    <script type="text/javascript" src="../chartjs/js/chart.min.js"></script>
    <script type="text/javascript" src="../chartjs/js/app.js"></script>

    

    </body>
</html>