<?php 
session_start();

if(!isset($_SESSION['clientlogin'])){
    header("Location: clientlogin.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="dist/img/logo.png">
  <title>Client | Aqua_Gauge</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

      
  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/album/">
    
    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://kit.fontawesome.com/5a4db65dc4.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="client_script2.js"></script>
    <script type="text/javascript" src="tank_script.js"></script>
    

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.0/examples/album/album.css" rel="stylesheet">

    <!-- Own CSS -->
    <link rel="stylesheet" type="text/css" href="css/gui_index.css">
    <link rel="stylesheet" type="text/css" href="css/water_animation.css">
    <link rel="stylesheet" href="tank.css">

    <style type="text/css">
          #chart-container {
              width: 640px;
              height: 500px;
          }
          .bs-example{
            margin: 20px;
              position: relative;
          }
    </style>
    <script>
        $(document).ready(function(){
            $(".show-toast").click(function(){
                $("#myToast").toast('show');
            });
        });
    </script> 

    <script>
          var myVar;

          function changing_height() {
          myVar = setInterval(change_height, 3000);
          }

          function change_height(){
              
              var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                  //alert(this.responseText);
                  var waterObject = JSON.parse(this.responseText);
                  var c=document.getElementById("inside_div");
                  c.style.height=220-waterObject.level + "px";
                  var level1=waterObject.level;
                 // alert (level1);
                  level=(level1/200)*100;
                 
                  document.getElementById("water_alert").innerHTML="The tank is "+level +"% full";
                  level=" ";
                  
                
              }
              };
              xhttp.open("GET", "tank.php?water_level", true);
              xhttp.send();

          }
      </script> 

</head>
<body class="hold-transition sidebar-mini layout-fixed" style="font-family: Quicksand;" onload="changing_height()">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="dist/img/logo.png" alt="Logo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#my_tank" class="nav-link">My_Tanks</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#info" class="nav-link">My_Info</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#vendor" class="nav-link">Water</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#contacts" class="nav-link">Contacts</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
      <button type="button" class="btn btn-outline-danger" style="padding:0px;"> 
            <a href="client_logout.php" class="nav-link" style="text-decoration:none; color:black;">Log out</a>
      </button>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/logo.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Aqua_Gauge</span> <br>
    </a>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="http://localhost/aqua_gauge2/client.php" class="nav-link active">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Client
                <i class="right fas fa-angle-right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item menu-open">
                <a href="#my_tank" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                  My_Tank
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#info" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>My_Info</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#vendor" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Water Vendor</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#contacts" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                  <p>Contacts</p>
                </a>
              </li>
              <li class="nav-item menu-open">
                <a href="http://localhost/aqua_gauge2/index.php" class="nav-link">
                <i class="nav-icon fas fa-home"></i>
                <p>
                Home
                </p>
                </a>
            </li> 
            </ul>
          </li>  
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Welcome <?php echo $_SESSION['first_name']." ".$_SESSION['last_name']?></h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
<!-- Main body - Tank section -->
<main role="main">

  <div class="container my-5">
    <div class="row p-4 pb-0 pe-lg-0 pt-lg-5 align-items-center rounded-3 border shadow-lg">
      <div class="col-lg-12 p-3 p-lg-10 pt-lg-8">
        <h1 class="display-6 fw-bold  text-center" id="my_tank">My_Tank</h1>
          <div class="container-fluid">
            <form class="display-6 fw-bold  text-center col-8 col-lg-5 ml-auto mr-auto">
              <?php
              // include database connection
              include 'db2.php';
              $query = "SELECT data.water_level, client_sensor.client_id FROM data INNER JOIN client_sensor
              ON data.sensor_id=client_sensor.sensor_id ORDER BY data_id DESC LIMIT 1";
              $results = mysqli_query($db, $query);
            
              
              while ($row=mysqli_fetch_array($results)) {
                  
                if ($row['client_id']==$_SESSION['client_id']){

                    if ($row['water_level']<=40){
                    echo "<div class='alert alert-danger' id='water_alert' style='font-size: 25px;'>"."The tank has ".$row['water_level']." litres"."</div>";
                    //include 'sms.php';
                  }
                    else if ($row['water_level']>40 && $row['water_level']<70){
                    echo "<div class='alert alert-info' id='water_alert' style='font-size: 25px;'>"."The tank is ".$row['water_level']." litres"."</div>";
                    }
                    else {
                      echo "<div class='alert alert-success' id='water_alert' style='font-size: 25px;'>".
                      "The tank is ".$row['water_level']." litres"."</div>";
                    }                
              }
             
            }
            
              //include 'sms.php';
              //header('location: client.php');
              ?> 
            </form>
          </div>
      </div>
      
        <table class="col-lg-12 p-lg-10 pt-lg-8 ml-auto mr-auto" style="height:300px;">
          <tr>            
            <td >
              <div id="main_div">
                  <div id="inside_div"></div>
              </div>
            </td>

            <td id="chart-container">
                <canvas id="mycanvas"></canvas>
            </td>
          </tr>
        </table>          
    
    </div>
  </div>

<!-- My_Info card -->
<div class="container-fluid mx-auto">
  <div class="col-lg-12 p-lg-10 pt-lg-8 ml-auto mr-auto ">
    <h1 class="display-6 fw-bold  text-center" id="info">My_Info</h1>
  </div>
    </div>
  <div class="card border-info text-center" style="max-width: 30%; margin-left: 35%; margin-right: 35%;margin-bottom: 50px;">
    <div class="card-header bg-info">
      <strong><h4>
      <?php 
      $client_id=$_SESSION['client_id'];
      //echo $client_id;
      echo "<button onclick=get_client_details(\"".$_SESSION['client_id']."\") data-bs-toggle='modal' data-bs-target='#exampleModal' name='edit_cust' class='btn btn-light'>
      <i class='fas fa-edit'></i> Edit
      </button>";
      ?>
       </h4></strong>
    </div>
    <div class="card-body">
      <h5 class="card-text"><strong>First name:</strong> <?php echo $_SESSION['first_name'];?> </h5>
      <h5 class="card-text"><strong>Last name:</strong> <?php echo $_SESSION['last_name'];?> </h5>
      <h5 class="card-text"><strong>Email:</strong> <?php echo $_SESSION['email'];?> </h5>
      <h5 class="card-text"><strong>Phone number:</strong> <?php echo $_SESSION['phone_no'];?></h5>
      <h5 class="card-text"><strong>Location:</strong> <?php echo $_SESSION['location'];?> </h5> 
      <?php 
        include 'db2.php';
        $query = "SELECT vendor.vendor_id, vendor.first_name, vendor.last_name, 
        client_vendor.client_id, client_vendor.vendor_id FROM vendor INNER JOIN client_vendor
        ON vendor.vendor_id=client_vendor.vendor_id ORDER BY client_vendor_id DESC";
        $results = mysqli_query($db, $query);
        while ($row=mysqli_fetch_array($results)) {
          if ($row['client_id']==$_SESSION['client_id']){
          echo "<h5 class='card-text'><strong>Water vendor: </strong>".$row['first_name']." ".$row['last_name']."</div>";
        }
      }
      ?> 
      </h5>
    </div>
  </div>
  </main>
<!-- End of My_Info card -->

<!-- Edit Client Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel"></h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
    <form action="client_edit.php" method="POST">
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Client ID</label>
        <input type="text" type="hidden" class="form-control" name="c_id" id="c_id" placeholder="Enter your first name">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">National ID</label>
        <input type="text" class="form-control" name="n_id" id="n_id" placeholder="Enter your national ID">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">First name</label>
        <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter your first name">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Last name</label>
        <input type="text" class="form-control" name="lname" id="lname" placeholder="Enter your last name">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Email</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Phone number</label>
        <input type="text" class="form-control" name="pno" id="pno" placeholder="Enter your phone number">
    </div>
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Location</label>
        <select name='location' class="form-select" id="location">
            <option value="Kitengela">Kitengela</option>
            <option value="Isinya">Isinya</option>
            <option value="Kajiado town">Kajiado town</option>
        </select>
        <input name="cust_id" type="hidden" id="cust_id">
    </div> 
    <div class="form-group col-12 col-lg-10 ml-auto mr-auto">
        <label for="exampleFormControlInput1">Water vendor</label>
        <select name='vendor_id' class="form-select" id="vendor_id">
        <?php
            // include database connection
            include 'db2.php';
            $query = "SELECT * from vendor";
            $results = mysqli_query($db, $query);
            while ($row=mysqli_fetch_array($results)) {
              echo "<option value='".$row['vendor_id']."'>".$row['first_name']." ".$row['last_name']." "."-->"." ".$row['location']."</option>";
            }
        ?>  
        </select>
    </div>  
      </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      <button type="submit" name="edit_cust2" class="btn btn-primary">Save changes</button>
    </div>
    </form>
  </div>
</div>
</div>
<!-- End of Client Edit Modal -->

<!-- Vendor card -->
<div class="container-fluid mx-auto">
  <div class="col-lg-12 p-lg-10 pt-lg-8 ml-auto mr-auto ">
    <h1 class="display-6 fw-bold  text-center" id="vendor">Available water vendors</h1>
  </div>
    </div>
 <!-- Vendors table -->
<div class="container" style="margin-bottom: 50px;">
<div class="table-responsive table-striped table-bordered">
  <table class="table" id="vendorTable">
     
      <?php
        // include database connection
        include 'db2.php';
          // select all data

          $query = "SELECT * FROM vendor";
          $results = mysqli_query($db, $query);
          $num = mysqli_num_rows($results);
          //check if more than 0 record found
          if($num >= 1){
           
              // data from database will be here
              echo "<tr>";
                  echo "<th scope='col'>First name</th>";
                  echo "<th scope='col'>Last name</th>";
                  echo "<th scope='col'>Phone number</th>";
                  echo "<th scope='col'>Location</th>";
              echo "</tr>";
   

            }
            while ($row=mysqli_fetch_array($results)) {
              if ($row['location']== $_SESSION['location']){
                  echo "<tr>";
                      echo "<td>".$row['first_name']."</td>";
                      echo "<td>".$row['last_name']."</td>";
                      echo "<td>".$row['phone_no']."</td>";                    
                      echo "<td>".$row['location']."</td>";
                      echo "</tr>";
              }
              }    
   
?>                                     
  </table>
</div>
</div>
</div>
</div>
<!-- End of vendor table -->

  </main>
<!-- End of vendor card -->

<!-- Footer -->
<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" style="background-color: #212529;">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div class="carousel-inner" id="contacts">
    <div class="carousel-item active">
      <img src="img/contacts.png" width="180px" height="120px" class="d-block w-20" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h4>Call/SMS this number for assistance:</h4>
        <h5>0799448090</h5>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/gmail.png" width="180px" height="120px" class="d-block w-20" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h4>Send an email to:</h4>
        <h5>dskoome2@gmail.com</h5>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/twitter.png" width="180px" height="120px" class="d-block w-20" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h4>Twitter handle:</h4>
        <h5>@aqua_gauge</h5>
      </div>
    </div>
  </div>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>
     
  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
  <script>window.jQuery || document.write('<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"><\/script>')</script>
  <!--<script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
  <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
  <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>-->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>-->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard.js"></script>-->
<!--Own Javascript files-->
<script type="text/javascript" src="chartjs/js/jquery.min.js"></script>
<script type="text/javascript" src="chartjs/js/chart.min.js"></script>
<script type="text/javascript" src="chartjs/js/app.js"></script>
</body>
</html>
