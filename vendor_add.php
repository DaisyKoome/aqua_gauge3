<?php
session_start();

// initializing variables
$n_id3 = "";
$first_name3 = "";
$last_name3 = "";
$email3 = "";
$password3 = "";
$phone_no3 = "";
$location3 = "";
$status3 = "";
$error = array();

require 'db2.php';
// REGISTER USER
if (isset($_POST['vendor_add'])) {
  // receive all input values from the form
  $n_id3 = mysqli_real_escape_string($db, $_POST['n_id3']);
  $first_name3 = mysqli_real_escape_string($db, $_POST['first_name3']);
  $last_name3 = mysqli_real_escape_string($db, $_POST['last_name3']);
  $email3 = mysqli_real_escape_string($db, $_POST['email3']);
  $password3 = mysqli_real_escape_string($db, $_POST['password3']);
  $phone_no3 = mysqli_real_escape_string($db, $_POST['phone_no3']);
  $location3 = mysqli_real_escape_string($db, $_POST['location3']);
  $status3 = mysqli_real_escape_string($db, $_POST['status3']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  $error_detected=0;
  if (empty($n_id3)) { $error_detected=1; array_push($error, "National ID is required"); }
  if (empty($first_name3)) {$error_detected=1;  array_push($error, "First name is required"); }
  if (empty($last_name3)) {$error_detected=1;  array_push($error, "Last name is required"); }
  if (empty($email3)) {$error_detected=1;  array_push($error, "Email is required"); }
  if (empty($password3)) {$error_detected=1;  array_push($error, "Password is required"); }
  if (empty($phone_no3)) {$error_detected=1;  array_push($error, "Phone number is required"); }
  if (empty($location3)) {$error_detected=1;  array_push($error, "Location is required"); }
  if (empty($status3)) {$error_detected=1;  array_push($error, "Status is required"); }
  
  $_SESSION['error']=$error;
  if ($error_detected==1) {
    header("Location: index_vendors.php");
  }

  // first check the database to make sure
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM vendor WHERE email='$email3' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);

  if ($user) { // if user exists
    if ($user['email'] === $email3) {
      $error_detected=1;
      array_push($error, "Email already exists");
    }
  }
  $_SESSION['error']=$error;
  if ($error_detected==1) {
    header("Location: index_vendors.php");
  }

  if ($error_detected==0) {//encrypt the password before saving in the database
    $password3=md5($password3);
  	$query = "INSERT INTO vendor (national_id,first_name,last_name,email,password,phone_no,location,status)
  			  VALUES('$n_id3','$first_name3', '$last_name3','$email3','$password3','$phone_no3', '$location3','$status3')";
  	mysqli_query($db, $query) or die(mysqli_error($db));
      $_SESSION['email'] = $email3;
      $_SESSION['client']=1;
  	header('location: index_vendors.php');
    
  }
  
}  
// ...
?>