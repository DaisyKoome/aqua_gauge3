$(document).ready(function(){
    $.ajax({
      url: "http://localhost/aqua_gauge2/chartjs/php/data.php",
      method: "GET",
      success: function(data) {
        console.log(data);
        var water_level = [];
        var time = [];
  
        for(var i in data) {
          time.push("Time " + data[i].time);
          water_level.push(data[i].water_level);
        }
  
        var chartdata = {
          labels: time,
          datasets : [
            {
              label: 'Water level',
              backgroundColor: 'rgba(200, 200, 200, 0.75)',
              borderColor: 'rgba(200, 200, 200, 0.75)',
              hoverBackgroundColor: 'rgba(200, 200, 200, 1)',
              hoverBorderColor: 'rgba(200, 200, 200, 1)',
              data: water_level

            }
          ]
        };
  
        var ctx = $("#mycanvas");
  
        var barGraph = new Chart(ctx, {
          type: 'bar',
          data: chartdata
        });
      },
      error: function(data) {
        console.log(data);
      }
    });
  });