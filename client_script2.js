function get_client_details(client_id) {
 
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var myObj = JSON.parse(this.responseText);
       
        //alert (this.responseText);
    
        //console.log (this.responseText);
        document.getElementById("c_id").value = myObj.id;
        //console.log(myObj.id);
        document.getElementById("n_id").value = myObj.n_id;
        document.getElementById("fname").value = myObj.fname;
        document.getElementById("lname").value = myObj.lname;
        document.getElementById("email").value = myObj.email;
        document.getElementById("pno").value = myObj.pno;
        //alert(myObj.pno);
        document.getElementById("location").value = myObj.location;
        document.getElementById("vendor_id").value = myObj.vendor_id;
        document.getElementById("cust_id").value = myObj.id;


        //Display modal
       
        var exampleModal = document.getElementById('exampleModal');
        exampleModal.addEventListener('show.bs.modal', function (event) {
          // Button that triggered the modal
          var button = event.relatedTarget;
          // Extract info from data-bs-* attributes
          var recipient = button.getAttribute('data-bs-whatever');
          // If necessary, you could initiate an AJAX request here
          // and then do the updating in a callback.
          //
          // Update the modal's content.
          var modalTitle = exampleModal.querySelector('.modal-title');
          var modalBodyInput = exampleModal.querySelector('.modal-body input');

          modalTitle.textContent = 'Edit client ';
          modalBodyInput.value = recipient;
        });
      }
    };
    xhttp.open("GET", "client_ajax2.php?client_id=" + client_id, true);
    xhttp.send();
  }