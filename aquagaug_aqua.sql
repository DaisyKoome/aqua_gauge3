-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 30, 2021 at 10:16 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aquagaug_aqua`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `first_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `first_name`, `last_name`, `email`, `password`) VALUES
(1, 'Daisy', 'Koome', 'dskoome2@gmail.com', 'df4b892324bbb648f27734b55c206b4b'),
(2, 'helen', 'mwende', 'helen@gmail.com', '7a2eb41a38a8f4e39c1586649da21e5f'),
(3, 'John', 'Kibet', 'john@gmail.com', '527bd5b5d689e2c32ae974c6229ff785'),
(4, 'harry', 'otieno', 'harry@gmail.com', '3b87c97d15e8eb11e51aa25e9a5770e9');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `client_id` int(11) NOT NULL,
  `first_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `phone_no` varchar(14) DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`client_id`, `first_name`, `last_name`, `email`, `password`, `phone_no`, `location`, `status`) VALUES
(1, 'daisy', 'mitti', 'daisymitti@gmail.com', 'df4b892324bbb648f27734b55c206b4b', '+254799448090', 'nakuru', ''),
(2, 'ned', 'kamau', 'kimalinick@gmail.com', 'f68daad189b2fffd0b8cab5e36ec9d96', '+254798765432', 'juja', 'Denied'),
(3, 'ken', 'mutie', 'dskoome2@gmail.com', 'f632fa6f8c3d5f551c5df867588381ab', '+254799448090', 'kjd', 'Approved'),
(4, 'dee', 'dee', 'dskoome@gmail.com', 'f31f2f4e88b1f4c29a4542671b247f9b', '+254799448090', 'kjd', 'Pending'),
(5, 'diana', 'mitti', 'koomedaisy17@students.tukenya.ac.ke', '8744c53672906143e20538f6ac3deadb', '+254799448090', 'maua', 'Pending'),
(6, 'nick', 'kim', 'daisymitti@gmail.com', 'e2e42a07550863f8b67f5eb252581f6d', '+254799448090', 'kitui', NULL),
(7, 'client', 'seven', 'client7@gmail.com', '62608e08adc29a8d6dbc9754e659f125', '+254798755432', 'juja', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `client_sensor`
--

CREATE TABLE `client_sensor` (
  `client_sensor_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `sensor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_sensor`
--

INSERT INTO `client_sensor` (`client_sensor_id`, `client_id`, `sensor_id`) VALUES
(1, 2, 3),
(2, 3, 1),
(3, 4, 2),
(4, 1, 4),
(5, 6, 5),
(6, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `client_vendor`
--

CREATE TABLE `client_vendor` (
  `client_vendor_id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_vendor`
--

INSERT INTO `client_vendor` (`client_vendor_id`, `client_id`, `vendor_id`) VALUES
(1, 1, 2),
(2, 2, 3),
(3, 6, 1),
(4, 3, 4),
(5, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `data_id` int(11) NOT NULL,
  `sensor_id` int(11) DEFAULT NULL,
  `water_level` int(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sms_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`data_id`, `sensor_id`, `water_level`, `time`, `sms_flag`) VALUES
(1, 1, 77, '2021-06-28 10:42:26', 0),
(2, 2, 62, '2021-06-18 00:42:44', 0),
(3, 3, 54, '2021-06-18 00:42:56', 0),
(4, 4, 32, '2021-06-18 00:43:17', 0),
(5, 5, 34, '2021-06-18 08:22:45', 0),
(313, 6, 98, '2021-07-14 13:27:40', 0),
(320, 4, 22, '2021-07-15 05:16:25', 0),
(354, 4, 11, '2021-07-22 07:49:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sensor`
--

CREATE TABLE `sensor` (
  `sensor_id` int(11) NOT NULL,
  `serial_no` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sensor`
--

INSERT INTO `sensor` (`sensor_id`, `serial_no`) VALUES
(1, 'D001'),
(2, 'D002'),
(3, 'D003'),
(4, 'D004'),
(5, 'D005'),
(6, 'D006'),
(7, 'D007'),
(8, 'D008');

-- --------------------------------------------------------

--
-- Table structure for table `sensor_vendor`
--

CREATE TABLE `sensor_vendor` (
  `sensor_vendor_id` int(11) NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `sensor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sensor_vendor`
--

INSERT INTO `sensor_vendor` (`sensor_vendor_id`, `vendor_id`, `sensor_id`) VALUES
(1, 2, 3),
(2, 2, 1),
(3, 3, 4),
(4, 1, 5),
(5, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tank`
--

CREATE TABLE `tank` (
  `tank_id` int(11) NOT NULL,
  `sensor_id` int(11) DEFAULT NULL,
  `tank_name_alias` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tank`
--

INSERT INTO `tank` (`tank_id`, `sensor_id`, `tank_name_alias`) VALUES
(1, 1, 'kajiado'),
(2, 2, 'kitui'),
(3, 3, 'nakuru'),
(4, 4, 'kisumu');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendor_id` int(11) NOT NULL,
  `first_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `phone_no` varchar(14) DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendor_id`, `first_name`, `last_name`, `email`, `password`, `phone_no`, `location`, `status`) VALUES
(1, 'karen', 'oloo', 'karen@gmail.com', 'ba952731f97fb058035aa399b1cb3d5c', '+254712345678', 'kjd', 'Approved'),
(2, 'liam', 'maina', 'liammaina@gmail.com', '6d8c4d103f90154cc06dd75a71d061be', '+254799448090', 'nakuru', ''),
(3, 'daisy', 'koome', 'daisy@gmail.com', 'df4b892324bbb648f27734b55c206b4b', '+254799448090', 'kjd', 'Denied'),
(4, 'natalie', 'kimani', 'nataliekimani@gmail.com', 'f9e1f3ae15198f819fbc3449479401bf', '+254708739801', 'nakuru', 'Pending'),
(5, 'daisy', 'mitti', 'dee2@gmail.com', '8744c53672906143e20538f6ac3deadb', '+254708543211', 'maua', 'Denied'),
(6, 'vendor', 'kibet', 'yvonne@gmail.com', 'vendor@gmail.com', '+254790653211', 'kitale', 'Approved'),
(7, 'vendor', 'seven', 'vendor@gmail.com', '7c3613dba5171cb6027c67835dd3b9d4', '+254798765432', 'maua', 'Approved'),
(8, 'client', 'eight', 'vendor8@gmail.com', '62608e08adc29a8d6dbc9754e659f125', '+254798755432', 'nakuru', 'Pending'),
(9, 'vendor', 'nine', 'vendor9@gmail.com', '7c3613dba5171cb6027c67835dd3b9d4', '+254712345677', 'maua', 'Denied');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `client_sensor`
--
ALTER TABLE `client_sensor`
  ADD PRIMARY KEY (`client_sensor_id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `sensor_id` (`sensor_id`);

--
-- Indexes for table `client_vendor`
--
ALTER TABLE `client_vendor`
  ADD PRIMARY KEY (`client_vendor_id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`data_id`),
  ADD KEY `sensor_id` (`sensor_id`);

--
-- Indexes for table `sensor`
--
ALTER TABLE `sensor`
  ADD PRIMARY KEY (`sensor_id`);

--
-- Indexes for table `sensor_vendor`
--
ALTER TABLE `sensor_vendor`
  ADD PRIMARY KEY (`sensor_vendor_id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `sensor_id` (`sensor_id`);

--
-- Indexes for table `tank`
--
ALTER TABLE `tank`
  ADD PRIMARY KEY (`tank_id`),
  ADD KEY `sensor_id` (`sensor_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `client_sensor`
--
ALTER TABLE `client_sensor`
  MODIFY `client_sensor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `client_vendor`
--
ALTER TABLE `client_vendor`
  MODIFY `client_vendor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=355;

--
-- AUTO_INCREMENT for table `sensor`
--
ALTER TABLE `sensor`
  MODIFY `sensor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sensor_vendor`
--
ALTER TABLE `sensor_vendor`
  MODIFY `sensor_vendor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tank`
--
ALTER TABLE `tank`
  MODIFY `tank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `client_sensor`
--
ALTER TABLE `client_sensor`
  ADD CONSTRAINT `client_sensor_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `client_sensor_ibfk_2` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`sensor_id`) ON DELETE CASCADE;

--
-- Constraints for table `client_vendor`
--
ALTER TABLE `client_vendor`
  ADD CONSTRAINT `client_vendor_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `client_vendor_ibfk_2` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`vendor_id`) ON DELETE CASCADE;

--
-- Constraints for table `data`
--
ALTER TABLE `data`
  ADD CONSTRAINT `data_ibfk_1` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`sensor_id`) ON DELETE CASCADE;

--
-- Constraints for table `sensor_vendor`
--
ALTER TABLE `sensor_vendor`
  ADD CONSTRAINT `sensor_vendor_ibfk_1` FOREIGN KEY (`vendor_id`) REFERENCES `vendor` (`vendor_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sensor_vendor_ibfk_2` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`sensor_id`) ON DELETE CASCADE;

--
-- Constraints for table `tank`
--
ALTER TABLE `tank`
  ADD CONSTRAINT `tank_ibfk_1` FOREIGN KEY (`sensor_id`) REFERENCES `sensor` (`sensor_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
