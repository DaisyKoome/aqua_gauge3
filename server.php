<?php
session_start();

// initializing variables
$n_id = "";
$fname = "";
$lname    = "";
$email    = "";
$password    = "";
$pno    = "";
$location    = "";
$client_type    = "";
$errors = array();

require 'db2.php';

// REGISTER USER
if (isset($_POST['register'])) {
  // receive all input values from the form
  $n_id = mysqli_real_escape_string($db, $_POST['n_id']);
  $fname = mysqli_real_escape_string($db, $_POST['fname']);
  $lname = mysqli_real_escape_string($db, $_POST['lname']);
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password = mysqli_real_escape_string($db, $_POST['password']);
  $pno = mysqli_real_escape_string($db, $_POST['pno']);
  $location = mysqli_real_escape_string($db, $_POST['location']);
  $client_type = mysqli_real_escape_string($db, $_POST['client_type']);
  
  //echo $username.",".$email.",".$password_1;


  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  $error_detected=0;
  if (empty($n_id)) { $error_detected=1; array_push($errors, "National ID is required"); } 
  if (empty($fname)) { $error_detected=1; array_push($errors, "First name is required"); }
  if (empty($lname)) {$error_detected=1;  array_push($errors, "Last name is required"); }
  if (empty($email)) {$error_detected=1;  array_push($errors, "Email is required"); }
  if (empty($password)) {$error_detected=1;  array_push($errors, "Password is required"); }
  if (empty($pno)) {$error_detected=1;  array_push($errors, "Phone number is required"); }
  if (empty($location)) {$error_detected=1;  array_push($errors, "Location is required"); }
  

  $_SESSION['errors']=$errors;
  if ($error_detected==1) {
    header("Location: registration.php");
  }

  // first check the database to make sure
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM client WHERE email='$email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);

  if ($user) { // if user exists
    if ($user['email'] === $email) {
      $error_detected=1;
      array_push($errors, "email already exists");
    }
  }
  $_SESSION['errors']=$errors;
  if ($error_detected==1) {
    header("Location: registration.php");
  }

  $user_check_query = "SELECT * FROM vendor WHERE email='$email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);

  if ($user) { // if user exists
    if ($user['email'] === $email) {
      $error_detected=1;
      array_push($errors, "email already exists");
    }
  }
  $_SESSION['errors']=$errors;
  if ($error_detected==1) {
    header("Location: registration.php");
  }

  if ($error_detected==0 && $client_type=="client") {
  	$password = md5($password);//encrypt the password before saving in the database

  	$query = "INSERT INTO client (national_id, first_name,last_name,email,password,phone_no,location)
  			  VALUES('$n_id', '$fname', '$lname', '$email', '$password', '$pno', '$location')";
  	mysqli_query($db, $query) or die(mysqli_error($db));
  	$_SESSION['username'] = $email;
    //$_SESSION['clientlogin']=1;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: clientlogin.php');
    
  }
  
  if ($error_detected==0 && $client_type=="vendor") {
  	$password = md5($password);//encrypt the password before saving in the database

  	$query = "INSERT INTO vendor (national_id, first_name,last_name,email,password,phone_no,location)
  			  VALUES('$n_id', '$fname', '$lname', '$email', '$password', '$pno', '$location')";
  	mysqli_query($db, $query) or die(mysqli_error($db));
  	$_SESSION['username'] = $email;
    //$_SESSION['vendorlogin']=1;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: clientlogin.php');
    
  }

}  
// ...
// ... 

// LOGIN USER
if (isset($_POST['login'])) {
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password = mysqli_real_escape_string($db, $_POST['password']);
  $client_type = mysqli_real_escape_string($db, $_POST['client_type']);

  
  $error = array();
  $errors_detected = 0;

  if (empty($email)) {
    $errors_detected = 1;
  	array_push($error, "Email is required");
  }
  if (empty($password)) {
    $errors_detected = 1;
  	array_push($error, "Password is required");
  }
  
  
  $_SESSION['error']=$error;
  if ($errors_detected==1) {
    header("Location: clientlogin.php");
  }

  if ($errors_detected==0) {
  	$password = md5($password);
    //echo $email;
    
  	$query = "SELECT * FROM client WHERE email='$email' AND password='$password'";
  	$results = mysqli_query($db, $query);
  	if (mysqli_num_rows($results) >= 1) {
      $data=mysqli_fetch_array($results);
     
      $_SESSION['client_id'] = $data['client_id']; 
      $_SESSION['first_name'] = $data['first_name'];
      $_SESSION['last_name'] = $data['last_name'];
  	  $_SESSION['email'] = $data['email'];
      $_SESSION['phone_no'] = $data['phone_no'];
      $_SESSION['location'] = $data['location'];
      $_SESSION['status'] = $data['status'];
      $_SESSION['clientlogin']=1;
      
  	  header('Location: client.php');
  	}
    else if (mysqli_num_rows($results) == 0) {
      //die($password);
      $query = "SELECT * FROM vendor WHERE email='$email' AND password='$password'";
      $results = mysqli_query($db, $query);
      if (mysqli_num_rows($results) >= 1) {
        $data=mysqli_fetch_array($results);
        
        $_SESSION['vendor_id'] = $data['vendor_id'];      
        $_SESSION['first_name'] = $data['first_name'];
        $_SESSION['last_name'] = $data['last_name'];
        $_SESSION['email'] = $data['email'];
        $_SESSION['phone_no'] = $data['phone_no'];
        $_SESSION['location'] = $data['location'];
        $_SESSION['status'] = $data['status'];
        $_SESSION['vendorlogin']=1;
        //echo "thfyfy";
        header('Location: vendor.php');
      }
      else if (mysqli_num_rows($results) == 0) {
        $query = "SELECT * FROM admin WHERE email='$email' AND password='$password'";
        $results = mysqli_query($db, $query);
        if (mysqli_num_rows($results) >= 1) {
          $data=mysqli_fetch_array($results);
          $_SESSION['first_name'] = $data['first_name'];
          $_SESSION['last_name'] = $data['last_name'];
          $_SESSION['email'] = $data['email'];
          $_SESSION['adminlogin']=1;
          header('location: index_sensors.php');
        }
        if (mysqli_num_rows($results) == 0) {
          array_push($error, "Wrong username/password combination");
          $_SESSION['error']=$error;
          header('location: clientlogin.php');
        }
      }
  	}
  }
}

?>